## current versions
* fedora: 41
  * podman: 5.3.0
* podman-desktop: 1.17.1
  * the default kind-configuration as well as
  * projectcontour: 1.24.2
* kind: 0.27.0
  * kindest/node: 1.32.2
* helm: 3.17.2
* kubectl: 1.32.3
* kustomize: 5.6.0
