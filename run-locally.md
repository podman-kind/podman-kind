```shell
podman machine init --cpus 4 --memory 8192 --rootful --now podman-machine-default
export CONTAINER_CONNECTION=podman-machine-default-root
podman build -t podman-kind . # buildah doesn't respect $CONTAINER_CONNECTION
podman run -v $(pwd)/tests:/app/tests --rm -it --privileged podman-kind

create-cluster-with-ingress
kubectl create -k /app/tests/kustomize/overlays/kind/
podman run -v /app/tests/intellij-http-client:/workdir --workdir /workdir --rm --network host docker.io/jetbrains/intellij-http-client status.http -D --report
```
