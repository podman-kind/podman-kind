ARG PACKAGES="podman git bash-completion fuse-overlayfs"
ARG FEDORA_VERSION=41

FROM quay.io/fedora/fedora:${FEDORA_VERSION} AS kind-config
# this stage is used to download/create the configuration files used for installing a kind-cluster with an contour ingress and the default settings of podman desktop

ARG PODMAN_DESKTOP_VERSION=1.17.1

ADD https://github.com/containers/podman-desktop/raw/v${PODMAN_DESKTOP_VERSION}/extensions/kind/src/templates/create-cluster-conf.mustache /create-cluster-conf.mustache
ADD https://github.com/containers/podman-desktop/raw/v${PODMAN_DESKTOP_VERSION}/extensions/kind/src/resources/contour.yaml /contour.yaml

RUN dnf install -y --setopt=install_weak_deps=False \
    rubygem-mustache

RUN echo '{"clusterName":"kind-cluster","httpHostPort":"9090","httpsHostPort":"9443"}' | mustache - /create-cluster-conf.mustache > /kind-cluster-config.yaml
RUN rm /create-cluster-conf.mustache

FROM quay.io/fedora/fedora:${FEDORA_VERSION} AS dry-run
ARG PACKAGES
# invalidate layer cache
ADD "https://www.random.org/integers/?num=1&min=1&max=1000000000&col=1&base=10&format=plain&rnd=new" "/tmp/rand"
# tail is used to skip the line(s) about metadata-downloads with timestamps
RUN { dnf upgrade --refresh --assumeno || true; } | tail -3 > /tmp/dnf-upgrade-dryrun \
    && { dnf install --setopt=metadata_expire=never --setopt=install_weak_deps=False \
    ${PACKAGES} \
    --assumeno || true; } | tail -1 > /tmp/dnf-install-dryrun

FROM quay.io/fedora/fedora:${FEDORA_VERSION}
ARG PACKAGES

ARG KIND_VERSION=0.27.0
ARG HELM_VERSION=3.17.2
ARG KUBECTL_VERSION=1.32.3
ARG KUSTOMIZE_VERSION=5.6.0

LABEL org.opencontainers.image.authors="blaimi@blaimi.de"

# this busts the layer-cache if the dry-run was different to the run before
COPY --from=dry-run /tmp/dnf-upgrade-dryrun /tmp/dnf-upgrade-dryrun
COPY --from=dry-run /tmp/dnf-install-dryrun /tmp/dnf-install-dryrun
RUN rm -f /tmp/dnf-upgrade-dryrun /tmp/dnf-install-dryrun

RUN dnf upgrade -y --refresh && dnf install --setopt=metadata_expire=never -y --setopt=install_weak_deps=False \
    ${PACKAGES} \
    && dnf clean all

COPY containers.conf /etc/containers/containers.conf

# pull the default image to speed up cluster creation
# skipping for now until investigated why this is not working in gitlab-ci
#RUN <<KINDESTNODE
#podman pull "docker.io/$(curl -L https://raw.githubusercontent.com/kubernetes-sigs/kind/v${KIND_VERSION}/pkg/apis/config/defaults/image.go | grep -oP 'kindest/node:v[0-9.]+@sha256:[0-9a-f]+')"
#KINDESTNODE

RUN <<KIND
[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v${KIND_VERSION}/kind-linux-amd64
[ $(uname -m) = aarch64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v${KIND_VERSION}/kind-linux-arm64
chmod +x ./kind
mv ./kind /usr/local/bin/kind

kind completion bash > /etc/bash_completion.d/kind
chmod a+r /etc/bash_completion.d/kind
KIND

RUN <<HELM
[ $(uname -m) = x86_64 ] && curl -Lo ./helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz
[ $(uname -m) = aarch64 ] && curl -Lo ./helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-arm64.tar.gz
tar -zxf helm.tar.gz
mv linux-*/helm /usr/local/bin/helm

helm completion bash > /etc/bash_completion.d/helm
chmod a+r /etc/bash_completion.d/helm
HELM

RUN <<KUBECTL
[ $(uname -m) = x86_64 ] && curl -Lo ./kubectl "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
[ $(uname -m) = aarch64 ] && curl -Lo ./kubectl "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/arm64/kubectl"
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl

kubectl completion bash > /etc/bash_completion.d/kubectl
chmod a+r /etc/bash_completion.d/kubectl

echo 'alias k=kubectl' >>~/.bashrc
echo 'complete -o default -F __start_kubectl k' >>~/.bashrc
KUBECTL

RUN <<KUSTOMIZE
[ $(uname -m) = x86_64 ] && curl -Lo ./kustomize.tar.gz "https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz"
[ $(uname -m) = aarch64 ] && curl -Lo ./kustomize.tar.gz "https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_arm64.tar.gz"
tar -zxf kustomize.tar.gz
mv ./kustomize /usr/local/bin/kustomize

kustomize completion bash > /etc/bash_completion.d/kustomize
chmod a+r /etc/bash_completion.d/kustomize
KUSTOMIZE

COPY --from=kind-config /kind-cluster-config.yaml ./
COPY --from=kind-config /contour.yaml ./
COPY create-cluster-with-ingress.sh /usr/local/bin/create-cluster-with-ingress
