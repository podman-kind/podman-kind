#!/bin/bash

set -e

kind create cluster --config /kind-cluster-config.yaml
kubectl create -f /contour.yaml
kubectl rollout status -n projectcontour deployment.apps/contour --timeout=600s
kubectl rollout status -n projectcontour daemonset.apps/envoy --timeout=600s
kubectl wait pods -n projectcontour -l '!job-name' --for condition=Ready --timeout=600s
kubectl wait jobs -n projectcontour --all --for condition=Complete --timeout=600s
